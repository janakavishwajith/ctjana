const http = require('http');

const port = 8080;

http.createServer((req, res) => {

  const responseText = `Hello from ${req.client.remoteAddress}:${req.client.remotePort} \nto ${req.client.localAddress}:${req.client.localPort}`;
  
  res.setHeader('Content-Type', 'text/plain');
  res.end(responseText);

}).listen(port);