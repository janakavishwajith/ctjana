const ampqplib = require('amqplib');
const fs = require('fs');

const exchangeName = 'topic_logs';

const handleMsg = async () => {

    const connection = await ampqplib.connect('amqp://rabbitmq')
    const channel = await connection.createChannel();

    await channel.assertExchange(exchangeName, 'topic', {durable: true})
    const channelQueue = channel.assertQueue('', {exclusive: true})
    channel.bindQueue(channelQueue.queue, exchangeName, '#')
    console.log('Observer start listening to all topic');

    try{
        fs.unlinkSync('./output/output.txt');
    } catch(error) {
        console.log(error)
    }

    channel.consume(channelQueue.queue, message => {
        if(message.content) {
            // console.log('OBSE got the message', Buffer.toString(message.content));
        
            const newMessage = `${new Date().toISOString()} Topic ${message.fields.routingKey}: ${message.content}`;
            console.log('OBSE going to write ', newMessage)
            fs.appendFile('./output/output.txt', newMessage+'\n', (err) => {
                if(err) {
                    return console.log(err);
                }
                console.log(newMessage, ' written to the file');
            })

        }

    }, {noAck:true})
}

setTimeout( () => {handleMsg()}, 10000);