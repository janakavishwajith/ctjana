const ampqplib = require("amqplib");

const exchangeName = 'topic_logs';
const listen_topic = 'my.o';
const publish_topic = 'my.i';

const handleMsg = async () => {

    const connection = await ampqplib.connect('amqp://rabbitmq')
    const channel = await connection.createChannel();

    await channel.assertExchange(exchangeName, 'topic', {durable: true})
    const channelQueue = channel.assertQueue('', {exclusive: true})
    channel.bindQueue(channelQueue.queue, exchangeName, listen_topic)
    console.log('IMED start listening to topic', listen_topic);

    channel.consume(channelQueue.queue, message => {
        if(message.content) {
            console.log('IMED got the message', message.content);

            setTimeout(()=>{
                const newMessage = `Got ${message.content}`
                channel.publish(exchangeName, publish_topic, Buffer.from(newMessage));
                console.log('IMED Published message', newMessage);
            }, 1000);

        }

    }, {noAck:true})
}

setTimeout( () => {handleMsg()}, 10000);