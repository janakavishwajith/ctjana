const ampqplib = require("amqplib");

const exchangeName = 'topic_logs';
const routing_topic = 'my.o'

const sendMsg = async () => {
    const connection = await ampqplib.connect('amqp://rabbitmq')
    const channel = await connection.createChannel();

    await channel.assertExchange(exchangeName, 'topic', {durable: true})

    const timer = ms => new Promise(res => setTimeout(res, ms));

    for(let j = 1; j <= 3; j++) {
        let message = 'MSG_' + j;
        channel.publish(exchangeName, routing_topic, Buffer.from(message));
        console.log('Published message', message);
        await timer(3000);
    }
    
}

setTimeout(() => {sendMsg();}, 11000);

