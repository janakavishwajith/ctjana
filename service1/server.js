const http = require('http');

const port = 8000;

const options = {
  hostname: 'service2',
  port: 8080,
  path: '/',
  method: 'GET'
}

const server = http.createServer((req, res) => {
  // res.statusCode = 200;
  // res.setHeader('Content-Type', 'text/plain');
  // res.end('Hello World');

  let reponseFromServer2 = '';
  const server2Req = http.request(options, server2Res =>{
    server2Res.on('data', d=>{
      const responseText = `Hello from ${req.client.remoteAddress}:${req.client.remotePort} \nto ${req.client.localAddress}:${req.client.localPort}`;
      reponseFromServer2 = d;
      res.setHeader('Content-Type', 'text/plain');
      res.end(d + '\n' + responseText);
    })
  })

  server2Req.end();

}).listen(port);
